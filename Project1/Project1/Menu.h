#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>

#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"

class Menu
{
public:

	Menu();
	~Menu();

	void printMainMenu() const;
	void addShapeMenu() const;
	char getUserChoice() const;
	void validChoice(char choice, char min_option, char max_option) const;

	unsigned int printModificationMenu(std::vector <Shape*>& shapes);
	char whatToMomify();

	void deleteShape(std::vector <Shape*>& shapes, unsigned int shape_index) const;
	void  moveShape(std::vector <Shape*>& shapes, unsigned int shape_index) const;
	void getShapeDitales(std::vector <Shape*>& shapes, unsigned int shape_index) const;

	void  deleteAllShapes(std::vector <Shape*>& shapes) const;

	void addCircle(std::vector <Shape*>& shapes) const;
	void addArrow(std::vector <Shape*>& shapes) const;
	void addTriangle(std::vector <Shape*>& shapes) const;
	void addRectangle(std::vector <Shape*>& shapes) const;

private: 
	Canvas _canvas;
};

