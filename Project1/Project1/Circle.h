#pragma once

#include "Shape.h"
#include "Point.h"

#define PI 3.14

class Circle : public Shape
{
public:

	// Constructor
	Circle(const Point& center, double radius, const std::string& type, const std::string& name);

	// Destructor
	virtual ~Circle();

	// Getters
	const Point& getCenter() const;
	double getRadius() const;

	// Methods
	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

	// add fields
	// override functions if need (virtual + pure virtual)
	virtual double getArea() const override;
	virtual double getPerimeter() const override;
	virtual void move(const Point& other) override;

private:
	Point _center;
	double _radius;
};