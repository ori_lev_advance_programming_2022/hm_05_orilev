#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	
	// Constructor
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);

	// Destructor
	~Triangle();

	// Methods
	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

	// override functions if need (virtual + pure virtual)
	virtual double getArea() const override;
};