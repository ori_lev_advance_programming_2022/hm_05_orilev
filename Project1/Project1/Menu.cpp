#include "Menu.h"

#include <iostream>
#include <string>


#define CIRCLE_TYPE "Circle"
#define ARROW_TYPE "Arrow"
#define TRIANGLE_TYPE "Triangle"
#define RECTANGLE_TYPE "Rectangle"


Menu::Menu() 
{}

Menu::~Menu()
{}

/*
* function print the main menu
* input: none
* output: none
*/
void Menu::printMainMenu() const
{
	system("cls");

	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all of the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;
}


/*
* function print the menu for adding shapes
* input: none
* output: none
*/
void Menu::addShapeMenu() const
{
	system("cls");

	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;
}

/*
* function get the user choic
* input: none
* output: user choice (char)
*/
char Menu::getUserChoice() const
{
	char choice = ' ';

	choice = getchar();
	getchar(); // clean the buffer

	return choice;
}

/*
* function check if the choice is valid (in the range of the valid choices)
* input: the user choice (char), and the choices range, min (char), max (char)
* output: none (might exit)
*/
void Menu::validChoice(char choice, char min_option, char max_option) const
{
	if (((int)choice < (int)min_option) || ((int)choice > int(max_option)))
	{
		system("cls");
		std::cerr << "EROR: invlid choice!" << std::endl;
		system("pause");
		_exit(1);
	}
}


/*
* function create instans of Circle class
* input: shapes vector
* output: none
*/
void Menu::addCircle(std::vector <Shape*>& shapes) const
{
	double x;
	double y;
	double radius;
	std::string name;

	system("cls");
	std::cout << "Please enter X :" << std::endl;
	std::cin >> x;

	std::cout << "Please enter Y :" << std::endl;
	std::cin >> y;

	std::cout << "Please enter radius :" << std::endl;
	std::cin >> radius;

	std::cout << "Please enter the name of the shape :" << std::endl;

	getchar();
	// clean buffer
	std::getline(std::cin, name);



	Circle* c = new Circle(Point(x, y), radius, CIRCLE_TYPE, name);
	shapes.push_back(c);

	c->draw(this->_canvas);
}


/*
* function create instans of Arrow class
* input: shapes vector
* output: none
*/
void Menu::addArrow(std::vector <Shape*>& shapes) const
{
	double x_1 = 0;
	double y_1 = 0;

	double x_2 = 0;
	double y_2 = 0;

	std::string name;

	system("cls");
	std::cout << "Enter the X of point number : 1" << std::endl;
	std::cin >> x_1;

	std::cout << "Enter the Y of point number : 1" << std::endl;
	std::cin >> y_1;

	std::cout << "Enter the X of point number : 2" << std::endl;
	std::cin >> x_2;

	std::cout << "Enter the Y of point number : 2" << std::endl;
	std::cin >> y_2;

	getchar(); // clean buffer

	std::cout << "Enter the name of the shape :" << std::endl;
	std::getline(std::cin, name);

	

	Point p1(x_1, y_1);
	Point p2(x_2, y_2);

	Arrow* a = new Arrow(p1, p2, ARROW_TYPE, name);
	shapes.push_back(a);

	a->draw(this->_canvas);
}


/*
* function create instans of Triangle class
* input: shapes vector
* output: none
*/
void Menu::addTriangle(std::vector <Shape*>& shapes) const
{
	double x_1 = 0;
	double y_1 = 0;
	double x_2 = 0;
	double y_2 = 0;
	double x_3 = 0;
	double y_3 = 0;
	std::string name;

	system("cls");

	std::cout << "Enter the X of point number : 1" << std::endl;
	std::cin >> x_1;

	std::cout << "Enter the Y of point number : 1" << std::endl;
	std::cin >> y_1;

	std::cout << "Enter the X of point number : 2" << std::endl;
	std::cin >> x_2;

	std::cout << "Enter the Y of point number : 2" << std::endl;
	std::cin >> y_2;

	std::cout << "Enter the X of point number : 3" << std::endl;
	std::cin >> x_3;

	std::cout << "Enter the Y of point number : 3" << std::endl;
	std::cin >> y_3;

	getchar(); //clean buffer

	std::cout << "Enter the name of the shape :" << std::endl;
	std::getline(std::cin, name);

	

	Point p1(x_1, y_1);
	Point p2(x_2, y_2);
	Point p3(x_3, y_3);

	Triangle* t = new Triangle(p1, p2, p3, TRIANGLE_TYPE, name);
	shapes.push_back(t);
	
	t->draw(this->_canvas);
}


void Menu::addRectangle(std::vector <Shape*>& shapes) const
{
	double x = 0;
	double y = 0;
	double width = 0;
	double height = 0;
	std::string name;

	system("cls");

	std::cout << "Enter the X of the to left corner :" << std::endl;
	std::cin >> x;

	std::cout << "Enter the Y of the top left corner :" << std::endl;
	std::cin >> y;

	std::cout << "Please enter the length of the shape :" << std::endl;
	std::cin >> height;

	std::cout << "Please enter the width of the shape :" << std::endl;
	std::cin >> width;

	std::cout << "Enter the name of the shape :" << std::endl;
	getchar(); // clean buffer

	std::getline(std::cin, name);

	

	Point p(x, y);

	myShapes::Rectangle* r = new myShapes::Rectangle(p, height, width, RECTANGLE_TYPE, name);
	shapes.push_back(r);

	r->draw(this->_canvas);
}


/*
* function print all the exist shapes add get the user choice of witch shape to modify
* input: the vectoer of the shapes
* output: the user shape choice (char)
*/
unsigned int Menu::printModificationMenu(std::vector <Shape*>& shapes)
{
	unsigned int choice = 0;

	system("cls");

	for (std::size_t i = 0; i < shapes.size(); ++i)
	{
		std::cout << "Enter " << i << " for " << shapes[i]->getName() << "(" << shapes[i]->getType() << ")" << std::endl;
	}
	std::cin >> choice;
	getchar();

	return choice;
}


/*
* function get the user choice of what to modify
* input: none
* output: user choice (char)
*/
char Menu::whatToMomify()
{
	char choice = ' ';

	system("cls");

	std::cout << "Enter 0 to move the shape" << std::endl;
	std::cout << "Enter 1 to get its details." << std::endl;
	std::cout << "Enter 2 to remove the shape." << std::endl;

	choice = getchar();
	getchar();

	return choice;
}

/*
* function delete all the shapes, and then draw all the shapes except the shape to delete (remove from board, not delete)
* input: shapes vector, index of the shape.
* output: none
*/
void Menu::deleteShape(std::vector <Shape*>& shapes, unsigned int shape_index) const
{
	for (std::size_t i = 0; i < shapes.size(); ++i)
	{
		shapes[i]->clearDraw(this->_canvas);
	}

	for (std::size_t i = 0; i < shapes.size(); ++i)
	{
		shapes[i]->clearDraw(this->_canvas);
		if (i != shape_index)
		{
			shapes[i]->draw(this->_canvas);
		}
	}

}

/*
* function move shape
* input: shapes vector, shape index
* output: none
*/
void Menu::moveShape(std::vector<Shape*>& shapes, unsigned int shape_index) const
{
	double x = 0;
	double y = 0;

	std::cout << "Please enter the X moving scale: ";
	std::cin >> x;

	std::cout << "Please enter the Y moving scale: ";
	std::cin >> y;

	getchar(); // clear buffer
	Point p(x, y);

	this->deleteShape(shapes, shape_index);

	shapes[shape_index]->move(p);

	shapes[shape_index]->draw(this->_canvas);
}

/*
* function delete all the shapes (board & vector)
* input: vector of shapes
* output: none
*/
void Menu::deleteAllShapes(std::vector<Shape*>& shapes) const
{
	for (std::size_t i = 0; i < shapes.size(); ++i)
	{
		shapes[i]->clearDraw(this->_canvas);
	}
	
	shapes.clear();
}

/*
* function print shape detailes
*/
void Menu::getShapeDitales(std::vector<Shape*>& shapes, unsigned int shape_index) const
{
	system("cls");
	shapes[shape_index]->printDetails();
	system("pause");
}