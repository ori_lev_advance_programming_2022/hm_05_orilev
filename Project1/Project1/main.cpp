#include "Menu.h"

#include <iostream>


#define ADD_NEW_SHAPE '0'
#define MODIFY_SHAPE '1'
#define REMOVE_ALL_SHAPES '2'
#define EXIT '3'

#define CIRCLE '0'
#define ARROW '1'
#define TRIANGLE '2'
#define RECTANGLE '3'

#define MOVE '0'
#define DETIALS '1'
#define REMOVE '2'

int main()
{
	std::vector <Shape*> shapes;
	std::vector <Shape*> temp_shapes;
	char choice = ' ';
	unsigned int shape_index = 0;

	char inner_choice = ' ';

	Menu* menu = new Menu();

	do {
		menu->printMainMenu();
		choice = menu->getUserChoice();
		menu->validChoice(choice, ADD_NEW_SHAPE, EXIT);

		switch (choice)
		{
		case ADD_NEW_SHAPE:
			menu->addShapeMenu();
			inner_choice = menu->getUserChoice();
			menu->validChoice(inner_choice, CIRCLE, RECTANGLE);

			switch (inner_choice)
			{
			case CIRCLE:
				menu->addCircle(shapes);
				break;
			case ARROW:
				menu->addArrow(shapes);
				break;
			case TRIANGLE:
				menu->addTriangle(shapes);
				break;
			case RECTANGLE:
				menu->addRectangle(shapes);
				break;
			}
			break;

		case MODIFY_SHAPE:

			shape_index = menu->printModificationMenu(shapes);

			if (shape_index < 0 || shape_index >= shapes.size())
			{
				std::cerr << "Error: Invalid number!" << std::endl;
				_exit(1);
			}

			inner_choice = menu->whatToMomify();

			switch (inner_choice)
			{
			case MOVE:
				menu->moveShape(shapes, shape_index);
				break;
			case DETIALS:
				menu->getShapeDitales(shapes, shape_index);
				break;
			case REMOVE:
				menu->deleteShape(shapes, shape_index);
				shapes.erase(shapes.begin() + shape_index);
			}
			break;

		case REMOVE_ALL_SHAPES:
			menu->deleteAllShapes(shapes);
			break;

		case EXIT:
			break;
		}
	} while (choice != EXIT);

	shapes.clear();
	temp_shapes.clear();
	delete menu;

	return 0;
}