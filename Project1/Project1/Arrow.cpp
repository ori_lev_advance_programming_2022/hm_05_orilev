#include "Arrow.h"

#include <iostream>


Arrow::Arrow(Point& a, Point& b, const std::string& type, const std::string& name) : 
	Shape(name, type),
	_source(a),
	_destination(b)
{
	if ((a.getX() == b.getX()) && (a.getY() == b.getY()))
	{
		std::cerr << "Arrow must get different points!" << std::endl;
		_exit(1);
	}
}


Arrow::~Arrow()
{
}


void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_source, _destination);
}


void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_source, _destination);
}


double Arrow::getPerimeter() const
{
	return _source.distance(_destination);
}

double Arrow::getArea() const
{
	return 0.0;
}


void Arrow::move(const Point& other)
{
	this->_source += other;
	this->_destination += other;
}


