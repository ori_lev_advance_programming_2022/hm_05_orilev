#include "Point.h"
#include <math.h>


#define CENTER 0


Point::Point()
{
	this->_x = CENTER;
	this->_y = CENTER;
}

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}


Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point(){}


Point Point::operator+(const Point& other) const
{
	Point p(this->_x + other._x, this->_y + other._y);
	return p;
}

Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other)
{
	return sqrt(pow(this->_x - other._x, 2) + pow(this->_y - other._y, 2));
}

