#include "Circle.h"

#define PI 3.14


Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) :
	Shape(name, type),
	_center(center),
	_radius(radius)
{
}

Circle::~Circle()
{
}

const Point& Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

double Circle::getArea() const
{
	return PI * this->_radius * this->_radius;
}

double Circle::getPerimeter() const
{
	return 2 * PI * this->_radius;
}

void Circle::move(const Point& other)
{
	this->_center += other;
}


