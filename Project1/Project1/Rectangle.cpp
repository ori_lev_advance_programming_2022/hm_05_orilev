#include "Rectangle.h"

#include <iostream>


myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	if (width == 0 || length == 0)
	{
		std::cerr << "ERORR: length or width can not be equal to zero!" << std::endl;
		_exit(1);
	}
	this->_points.push_back(a);
	this->_points.push_back(Point(a.getX() + abs(length), a.getY() + abs(width)));
}

myShapes::Rectangle::~Rectangle()
{
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

double myShapes::Rectangle::getArea() const
{
	
	double width = abs(_points[0].getY() - _points[1].getY());
	double hight = abs(_points[0].getX() - _points[1].getX());

	return width * hight;
}


