#include "Triangle.h"

#include <iostream>

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	if (((a.getX() == b.getX()) && (b.getX() == c.getX())) || ((a.getY() == b.getY()) && (b.getY() == c.getY())))
	{
		std::cerr << "Erorr, can't create triangle when all points on the same line!" << std::endl;
		_exit(1);
	}
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

Triangle::~Triangle()
{
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

double Triangle::getArea() const
{
	double area = 0;
	
	area += abs(this->_points[0].getX() * (this->_points[2].getY() - this->_points[1].getY()));
	area += abs(this->_points[1].getX() * (this->_points[0].getY() - this->_points[2].getY()));
	area += abs(this->_points[2].getX() * (this->_points[1].getY() - this->_points[0].getY()));
	
	area /= 2;
	return area;
}
