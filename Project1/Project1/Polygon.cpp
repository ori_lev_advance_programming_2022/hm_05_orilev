#include "Polygon.h"

Polygon::Polygon(const std::string& type, const std::string& name):
	Shape(name, type)
{
}

Polygon::~Polygon()
{
}

double Polygon::getPerimeter() const
{
	double sum = 0;
	Point p;
	Point p_2;

	for (std::size_t i = 0; i < this->_points.size() - 1; ++i)
	{
		p = this->_points[i];
		p_2 = this->_points[i + 1];

		sum += p.distance(p_2);
	}
	
	p = this->_points[this->_points.size() - 1];
	p_2 = this->_points[0];
	
	sum += p.distance(p_2);

	return sum;
}

void Polygon::move(const Point& other)
{
	for (std::size_t i = 0; i < this->_points.size(); ++i)
	{
		this->_points[i] += other;
	}
}
