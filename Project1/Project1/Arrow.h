#pragma once
#include "Shape.h"

class Arrow : public Shape
{
public:

	// Constructor
	Arrow(Point& a, Point& b, const std::string& type, const std::string& name);

	// Destructor
	virtual ~Arrow();

	// Methods
	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

	// override functions if need (virtual + pure virtual)
	virtual double getPerimeter() const;
	virtual double getArea() const;
	virtual void move(const Point& other);

private:
	Point& _source;
	Point& _destination;
};