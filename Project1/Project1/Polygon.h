#pragma once

#include "Shape.h"
#include <vector>

class Polygon : public Shape
{
public:

	// Constructor
	Polygon(const std::string& type, const std::string& name);

	// Destructor
	virtual ~Polygon();

	// Methods
	virtual double getPerimeter() const override;
	
	// override functions if need (virtual + pure virtual)
	virtual void move(const Point& other) override;

protected:
	std::vector<Point> _points;
};