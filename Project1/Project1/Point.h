#pragma once

class Point
{
public:

	// Constructors
	Point(); // initialize values to 0
	Point(double x, double y);
	Point(const Point& other);

	// Destructor
	virtual ~Point();
	
	// Operators
	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	// Getters
	double getX() const;
	double getY() const;

	// Methods
	double distance(const Point& other);

private:
	double _x;
	double _y;
};